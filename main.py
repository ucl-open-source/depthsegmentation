import rgbdSyncCam
import cv2
import numpy as np

## Functions
def resizeImg(img, scale):
    width = int(img.shape[1] * scale)
    height = int(img.shape[0] * scale)
    dim = (width, height)
    return cv2.resize(img, dim, interpolation = cv2.INTER_AREA)

## Settings
threshold = 2000 # mm

## Setup
cam = rgbdSyncCam.RGBDSyncCam()
dMaskMin = None

## Delay for camera to set (potential temporal filtering)
delayCounter = 0
delayFrame = None
while delayCounter < 64:
    delayFrame = cam.getFrames()
    if not delayFrame:
        continue
    delayCounter += 1

## Get initial depth subMask
temporalCounterMax = 64
temporalImages = np.zeros(shape=delayFrame[1].shape + (temporalCounterMax,))
temporalCounter = 0
while temporalCounter < temporalCounterMax:
    frames = cam.getFrames()
    if not frames:
        continue
    temporalImages[:,:,temporalCounter] = frames[1]
    temporalCounter += 1

dMaskMin = np.median(temporalImages, axis=2)
dMaskMin = dMaskMin.astype(np.uint16)
dMaskMinShow = cv2.normalize(dMaskMin, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)

#cv2.imshow("dMaskMin", resizeImg(dMaskMin, 0.8))
#cv2.imshow("dMaskMin", dMaskMinShow)

## Process loop
while True:
    frames = cam.getFrames()
    if not frames:
        continue

    #cv2.imshow("Color", resizeImg(frames[0], 0.4))
    #cv2.imshow("Depth", resizeImg(frames[1], 0.4))
    #cv2.imshow("Disparity", resizeImg(frames[2], 0.4))

    
    #difference = np.zeros(dMaskMin.shape, dtype=np.float)
    difference = np.subtract(dMaskMin, frames[1], dtype=float)
    #print("Diff 0:",frames[1][0,0], dMaskMin[0,0], difference[0,0])
    difference = np.clip(difference, 0, 65535)
    #print("Diff 1:",frames[1][0,0], dMaskMin[0,0], difference[0,0])
    #difference = np.abs(difference)
    difference = difference.astype(np.uint16)
    #difference = np.diff([frames[1], dMaskMin], axis=2)
    differenceShow = cv2.normalize(difference, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
    #print(difference.size, difference.dtype)
    #cv2.imshow("Difference", resizeImg(differenceShow, 0.8))
    #print("Diff 2:", frames[1][0,0], dMaskMin[0,0], difference[0,0])

    _,mask = cv2.threshold(difference, threshold, 65535, cv2.THRESH_BINARY) # 65535
    maskShow = cv2.normalize(mask, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
    #cv2.imshow("Mask", resizeImg(mask, 0.4))

    closeKernel = np.ones((5, 5), np.uint8)
    closeIterations = 6
    eroded = cv2.erode(mask, closeKernel, iterations=closeIterations)
    #cv2.imshow("Eroded", resizeImg(eroded, 0.4))
    dilated = cv2.dilate(eroded, closeKernel, iterations=closeIterations)
    cv2.imshow("Dilated", resizeImg(dilated, 0.4))

    print("dil shape:", dilated.shape, "rgb shape:", frames[0].shape)
    rgbMask = cv2.normalize(dilated, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
    maskedRGB = cv2.bitwise_and(frames[0], frames[0], mask=rgbMask)
    cv2.imshow("maskedRGB", resizeImg(maskedRGB, 0.8))

    keyIn = cv2.waitKey(10)
    if keyIn == ord("q"):
        break
