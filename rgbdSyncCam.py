import depthai as dai

class RGBDSyncCam:
    def __init__(self) -> None:
        # Sync var
        self.msgs = dict()
        
        # Settings
        downscaleColor = True
        fps = 30
        monoResolution = dai.MonoCameraProperties.SensorResolution.THE_720_P

        # Create pipeline
        pipeline = dai.Pipeline()
        self.device = dai.Device()

        # Define sources and outputs
        camRgb = pipeline.create(dai.node.ColorCamera)
        left = pipeline.create(dai.node.MonoCamera)
        right = pipeline.create(dai.node.MonoCamera)
        stereo = pipeline.create(dai.node.StereoDepth)

        rgbOut = pipeline.create(dai.node.XLinkOut)
        disparityOut = pipeline.create(dai.node.XLinkOut)
        depthOut = pipeline.create(dai.node.XLinkOut)
        rgbOut.setStreamName("rgb")
        disparityOut.setStreamName("disp")
        depthOut.setStreamName("depth")

        #Properties
        camRgb.setBoardSocket(dai.CameraBoardSocket.RGB)
        camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
        camRgb.setFps(fps)
        if downscaleColor: camRgb.setIspScale(2, 3)
        # For now, RGB needs fixed focus to properly align with depth.
        # This value was used during calibration
        try:
            calibData = self.device.readCalibration2()
            lensPosition = calibData.getLensPosition(dai.CameraBoardSocket.RGB)
            if lensPosition:
                camRgb.initialControl.setManualFocus(lensPosition)
        except:
            raise
        left.setResolution(monoResolution)
        left.setBoardSocket(dai.CameraBoardSocket.LEFT)
        left.setFps(fps)
        right.setResolution(monoResolution)
        right.setBoardSocket(dai.CameraBoardSocket.RIGHT)
        right.setFps(fps)

        # Stereo settings and post-processing
        stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_DENSITY)
        stereo.initialConfig.setMedianFilter(dai.MedianFilter.KERNEL_5x5)
        stereo.setLeftRightCheck(True) # LR-check is required for depth alignment
        stereo.setDepthAlign(dai.CameraBoardSocket.RGB)

        config = stereo.initialConfig.get()
        #config.postProcessing.speckleFilter.enable = True
        #config.postProcessing.speckleFilter.speckleRange = 50
        config.postProcessing.temporalFilter.enable = True
        config.postProcessing.temporalFilter.persistencyMode = dai.RawStereoDepthConfig.PostProcessing.TemporalFilter.PersistencyMode.VALID_2_IN_LAST_4
        #config.postProcessing.spatialFilter.enable = True
        #config.postProcessing.spatialFilter.holeFillingRadius = 2
        #config.postProcessing.spatialFilter.numIterations = 1
        #config.postProcessing.thresholdFilter.minRange = 400
        #config.postProcessing.thresholdFilter.maxRange = 15000
        config.postProcessing.decimationFilter.decimationMode = dai.RawStereoDepthConfig.PostProcessing.DecimationFilter.DecimationMode.NON_ZERO_MEAN
        config.postProcessing.decimationFilter.decimationFactor = 2
        stereo.initialConfig.set(config)
        
        # Linking
        camRgb.isp.link(rgbOut.input)
        left.out.link(stereo.left)
        right.out.link(stereo.right)
        stereo.disparity.link(disparityOut.input)
        stereo.depth.link(depthOut.input)

        # Start pipeline
        self.device.startPipeline(pipeline)

    def __add_msg(self, msg, name, seq = None):
        if seq is None:
            seq = msg.getSequenceNum()
        seq = str(seq)
        if seq not in self.msgs:
            self.msgs[seq] = dict()
        self.msgs[seq][name] = msg

    def __get_msgs(self):
        seq_remove = [] # Arr of sequence numbers to get deleted
        for seq, syncMsgs in self.msgs.items():
            seq_remove.append(seq) # Will get removed from dict if we find synced msgs pair
            # Check if we have both detections and color frame with this sequence number
            if len(syncMsgs) == 3: # rgb + depth + disp
                for rm in seq_remove:
                    del self.msgs[rm]
                return syncMsgs # Returned synced msgs
        return None

    def getFrames(self):
        for name in ['rgb', 'disp', 'depth']:
            msg = self.device.getOutputQueue(name).tryGet()
            if msg is not None:
                self.__add_msg(msg, name)

        synced = self.__get_msgs()
        if synced:
            frameRgb = synced["rgb"].getCvFrame()
            frameDisp = synced["disp"].getFrame()
            frameDepth = synced["depth"].getFrame()
            return [frameRgb, frameDepth, frameDisp]
        return None